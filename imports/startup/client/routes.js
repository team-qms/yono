import React from 'react';
import { Router, Route, Switch } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';

// route components
import AppContainer from '../../ui/App.js';
import QueueDetailsContainer from '../../ui/QueueDetails.js';
import QueueJoinContainer from '../../ui/QueueJoin.js';
import ClientDetailsContainer from '../../ui/ClientDetails.js';


const browserHistory = createBrowserHistory();

//The api routes using react router
export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route exact path="/" component={AppContainer}/>
      <Route exact path="/queue/:id" component={QueueDetailsContainer}/>
      <Route exact path="/queue/:queueId/join" component={QueueJoinContainer}/>
      <Route exact path="/queue/:queueId/client/:clientId" component={ClientDetailsContainer}/>
    </Switch>
  </Router>
);
