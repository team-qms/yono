import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';


export const Queues = new Mongo.Collection('queues');

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('queues', function queuesPublication() {
    return Queues.find({
      $or: [
        //{ private: { $ne: true } },
        { owner: this.userId },
        //{ queueUrl: queueUrl},
        
      ],
    });
  });
}

Meteor.methods({
  'queues.insert'(text) {
    check(text, String);

    // Make sure the user is logged in before inserting a queue
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    Queues.insert({
      text,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.users.findOne(this.userId).username,
    });
  },
  'queues.remove'(queueId) {
    check(queueId, String);

    const queue = Queues.findOne(queueId);
    if (queue.owner !== this.userId) {
      // If the queue is private, make sure only the owner can delete it
      throw new Meteor.Error('not-authorized');
    }

    Queues.remove(queueId);
  },
});
