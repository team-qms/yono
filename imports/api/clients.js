import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
export const Clients = new Mongo.Collection('clients');

export const statusWait = 'WAIT'
export const statusServing = 'SERVING'
export const statusServed = 'SERVED'
export const statusLeft = 'LEFT'

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish clients that are public or belong to the current user
  Meteor.publish('clients', function clientsPublication() {
    return Clients.find();
  });
}

Meteor.methods({
  'clients.insert'(queueId, status) {
    check(queueId, String);
    check(status, String);

    return Clients.insert({
      createdAt: new Date(),
      owner: this.userId,
      queueId,
      status:status? status:statusWait
    });
  },
  'clients.remove'(clientId) {
    check(clientId, String);

    const client = Clients.findOne(clientId);
    if (client.owner !== this.userId) {
      // If the client is private, make sure only the owner can delete it
      throw new Meteor.Error('not-authorized');
    }

    Clients.remove(clientId);
  },
  'clients.setStatus'(clientId, statusValue) {
    check(clientId, String);
    check(statusValue, String);

    const client = Clients.findOne(clientId);

    Clients.update(clientId, { $set: { status: statusValue } });
   },
});
