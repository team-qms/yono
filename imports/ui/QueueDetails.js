import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { withStyles } from '@material-ui/core/styles';
import { withTracker } from 'meteor/react-meteor-data';
import { withRouter } from 'react-router-dom'
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import FaceIcon from '@material-ui/icons/Face';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Collapse from '@material-ui/core/Collapse';
import red from '@material-ui/core/colors/red';
import Divider from '@material-ui/core/Divider';

import QueueMembers from './QueueMembers.js';

var QRCode = require('qrcode.react');

//TODO: Unify these styles at one place!!!
const styles = theme => ({
  card: {
    /*maxWidth: 400,*/
  },
  cardHeader:{
    /*backgroundImage: "url('images/ishan-seefromthesky-1113280-unsplash.jpg')"*/
    backgroundColor: 'rgb(136, 229, 239)'
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: '#498fad',
  },
  contentQR:{
    textAlign:'center'
  },
  contentMessage:{
    textAlign:'left'
  },
  contentQueue:{
    textAlign:'center'
  }
});

// QueueDetails component - shows details of single queue
class QueueDetails extends Component {
  state = { expanded: true };
  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };
  
  render() {
    const queueId_param = this.props.match.params.id;
    var queueUrl = window.location.href+'/join'
    console.log(queueUrl)
    const { classes } = this.props;
    return (
    <div className="container">
      <Card className={classes.card}>
          <CardHeader className={classes.cardHeader}
            avatar={
              <Avatar aria-label="Queue" className={classes.avatar}>
                Q
              </Avatar>
            }

            title={"Queue:"+this.props.match.params.id}
            subheader="createdAt"
          />
          <CardContent className={classes.contentQR} >
            <QRCode value={queueUrl}/>
            <Typography component="p" className={classes.contentMessage} >
              To join the queue, open a QR reading App on your phone and read the QR code given here or join at this url {queueUrl}.
            </Typography>
          </CardContent>
          <CardActions className={classes.actions} disableActionSpacing>
            <IconButton
              className={classnames(classes.expand, {
                [classes.expandOpen]: this.state.expanded,
              })}
              onClick={this.handleExpandClick}
              aria-expanded={this.state.expanded}
              aria-label="Show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </CardActions>
          <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
            <CardContent>
              <Typography paragraph>Members:</Typography>
              <div>
                <QueueMembers queueId={this.props.match.params.id}/>
              </div>
            </CardContent>
          </Collapse>
        </Card>
      </div>
    );
  }
}

QueueDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(withTracker(({queueIdParam}) => {
  const user = Meteor.isServer ? null : Meteor.user();
  return {
    currentUser: user,
  };
}) (QueueDetails)));
