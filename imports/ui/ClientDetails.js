import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { withStyles } from '@material-ui/core/styles';
import { withTracker } from 'meteor/react-meteor-data';
import { withRouter } from 'react-router-dom'

import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import FaceIcon from '@material-ui/icons/Face';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Collapse from '@material-ui/core/Collapse';
import red from '@material-ui/core/colors/red';
import Divider from '@material-ui/core/Divider';


import { Clients, statusWait, statusServing, statusServed, statusLeft } from '../api/clients.js';


//import { QRCode } from 'qrcode.react';
//var QRCode = require('qrcode.react');

//TODO: Unify these styles at one place, repeated alot!
const styles = theme => ({
  card: {
    /*maxWidth: 400,*/
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  cardHeader:{
    /*backgroundImage: "url('images/ishan-seefromthesky-1113280-unsplash.jpg')"*/
    backgroundColor: 'rgb(136, 229, 239)'
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: '#498fad',
  },
  contentQR:{
    textAlign:'center'
  },
  contentMessage:{
    textAlign:'left'
  },
  contentClient:{
    textAlign:'center'
  }
});

// ClientDetails component - represents a single clients details
class ClientDetails extends Component {
 
  leaveDisabled = false
  
  constructor(props) {
    super(props);
    this.messageLoading = "Loading your status."
    this.message = this.messageLoading
    
  }
  
  render_(){
      return(<p> you have joined the queue </p>);
  }
  
  leaveQueue(){
    console.log('leave clicked'+statusLeft);
    //Meteor.call('clients.remove', this.props.match.params.clientId);
    //this.props.client.status = statusLeft;
    Meteor.call('clients.setStatus', this.props.match.params.clientId, statusLeft);
    //this.message = this.messageLeft;
    //this.leaveDisabled = true
  }
  vibrate() {
    // Values at even indices (0, 2, 4, ...) specify vibrations, while the odd
    // indices specify pauses.
    // Vibrate for 500ms 6 times, pausing for 250ms in between each one.
    navigator.vibrate([500, 250, 500, 250, 500, 250, 500, 250, 500, 250, 500]);
  }
  render () {

    console.log(this.props.client);
    const { classes } = this.props;
   //It is taking some time to insert a client, wait for it and then render.
    console.log('rendering');
    if (this.props.client !== undefined) {
      this.message = 'Your status : ' + this.props.client.status;
      this.leaveDisabled = this.props.client.status === statusWait? false : true;

      //TODO: Mobile vibration feature to be include in android app

      //if (this.props.client.status === statusServed){
      //  //window.close()
      //var isMobile = (/iPhone|iPod|iPad|Android|BlackBerry/).test(navigator.userAgent);
      //  if (isMobile){
      //  this.vibrate();
      //  }
      //}
    }
   
    
    return (
    <div className="container">
      <Card className={classes.card}>
          <CardHeader className={classes.cardHeader}
            //background-image={'http://'+window.location.host+'/images/ishan-seefromthesky-1113280-unsplash.jpg'}
            avatar={
              <Avatar aria-label="Client" className={classes.avatar}>
                C
              </Avatar>
            }
            //action={
            //  <IconButton>
            //    <MoreVertIcon />
            //  </IconButton>
            //}
            title="Client"
            subheader="createdAt"
          />
          {/**<CardMedia
            className={classes.media}
            image={'http://'+window.location.host+'/images/ishan-seefromthesky-1113280-unsplash.jpg'}
            title="Client"
          ></CardMedia>**/}
          
          
          <CardContent className={classes.contentQR} >
            <Typography component="p" className={classes.contentMessage} >
              {this.message}
            </Typography>
          </CardContent>
          <CardActions className={classes.actions} disableActionSpacing>
            <Button variant="outlined" color="primary" onClick={this.leaveQueue.bind(this)} disabled={this.leaveDisabled}>
              Leave
            </Button>
            {/**<Button variant="outlined" color="primary" onClick={this.vibrate.bind(this)} hide='true'>
              Vibrate
            </Button>**/}
            
          </CardActions>
          
        </Card>
      </div>
    );
  }
}

ClientDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(withTracker((props) => {
  Meteor.subscribe('clients');
  //const user = Meteor.isServer ? null : Meteor.user();
  return {
    client: Clients.find({_id:props.match.params.clientId}).fetch()[0],
    clientCount: Clients.find({_id:props.match.params.clientId}).count(),
  };
}) (ClientDetails)));

