import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import classnames from 'classnames';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Queues } from '../api/queues.js';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import FaceIcon from '@material-ui/icons/Face';
import { withRouter } from 'react-router-dom'

// Queue component - represents a single queue item
class Queue extends Component {

  deleteThisQueue() {
    Meteor.call('queues.remove', this.props.queue._id);
  }
  
  showThisQueue(){
    this.props.history.push('queue/'+this.props.queue._id)
  }

  render() {
    return (
      <ListItem>
        <Avatar>
          <FaceIcon/>
        </Avatar>
        <ListItemText primary={this.props.queue.text} secondary='time'/>
        <Button variant="outlined" color="primary" onClick={this.showThisQueue.bind(this)}>
        View
        </Button>
        <Button variant="outlined" color="secondary" onClick={this.deleteThisQueue.bind(this)}>
        Delete
        </Button>
        
      </ListItem>

    );
  }
}
export default withRouter(Queue);
