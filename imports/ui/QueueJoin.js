import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { withStyles } from '@material-ui/core/styles';
import { withTracker } from 'meteor/react-meteor-data';
import { withRouter } from 'react-router-dom'

import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import FaceIcon from '@material-ui/icons/Face';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Collapse from '@material-ui/core/Collapse';
import red from '@material-ui/core/colors/red';
import Divider from '@material-ui/core/Divider';

//TODO: Unify these styles at one place!!!
const styles = theme => ({
  card: {
    maxWidth: 400,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
  contentQR:{
    textAlign:'center'
  },
  contentMessage:{
    textAlign:'left'
  },
  contentClient:{
    textAlign:'center'
  }
});

//http://jono-hadaytullah.c9users.io:8080/queue/QRnb2jvzm6sJ3Qq34/queue/QRnb2jvzm6sJ3Qq34/client/tsgMTkjWepe7y7Zyi/
class QueueJoin extends Component {
  constructor(props) {
    super(props);
    
    const queueId_param = this.props.match.params.queueId;
    var selfThis=this;
    const status ="WAIT"
    Meteor.call('clients.insert', queueId_param, status, function(error, newClientId){
        //selfThis.props.history.push('queue/'+queueId_param+'/client/'+newClient);   
        selfThis.props.history.push('client/'+newClientId);   
    });
    
    
  }
 
  render() {
    const { classes } = this.props;
    return (
    <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="Client" className={classes.avatar}>
              C
            </Avatar>
          }
          title="Client"
          subheader="createdAt"
        />
        
        <CardContent className={classes.contentQR} >
          <Typography component="p" className={classes.contentMessage} >
            Please wait, joining the queue..
          </Typography>
        </CardContent>
        
      </Card>
    );
    
  }
}

QueueJoin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles) (withRouter(QueueJoin));
