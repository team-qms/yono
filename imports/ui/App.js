import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import classnames from 'classnames';
import { Queues } from '../api/queues.js';
import Queue from './Queue.js';
import AccountsUIWrapper from './AccountsUIWrapper.js';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Button from '@material-ui/core/Button';
import FaceIcon from '@material-ui/icons/Face';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Collapse from '@material-ui/core/Collapse';
import red from '@material-ui/core/colors/red';
import Divider from '@material-ui/core/Divider';


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  card: {
    /*maxWidth: 400,*/
    minHeight:400
  },
  cardHeader:{
    /*backgroundImage: "url('images/ishan-seefromthesky-1113280-unsplash.jpg')"*/
    backgroundColor: 'rgb(136, 229, 239)'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
  auth:{
    textAlign:'left'
  },
  avatar: {
    backgroundColor: '#498fad',
  },

});

// App component - represents the whole app. Renders the main entry point UI.
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newQueueName:''
      //hideCompleted: false,
    };
  }

  handleSubmit(event) {
    event.preventDefault();
    Meteor.call('queues.insert', this.state.newQueueName);
    this.state.newQueueName = ''
  }

  toggleHideCompleted() {
    this.setState({
      hideCompleted: !this.state.hideCompleted,
    });
  }

  renderQueues() {
    let filteredQueues = this.props.queues;
    //if (this.state.hideCompleted) {
    //  filteredQueues = filteredQueues.filter(queue => !queue.checked);
    //}
    return filteredQueues.map((queue) => {
      const currentUserId = this.props.currentUser && this.props.currentUser._id;
      //const showPrivateButton = queue.owner === currentUserId;

      return (
        <Queue
          key={queue._id}
          queue={queue}
          //showPrivateButton={showPrivateButton}
        />
      );
    });
  }

  onNewQueueNameChange(e){
    //if (e.target.value){
    this.setState({newQueueName:e.target.value});
    //}
  }


  render() {
    //console.log(this.props.currentUser)
    const { classes } = this.props;
    return (
    <div className="container">
      <Card className={classes.card}>
          <CardHeader className={classes.cardHeader}
            avatar={
              <Avatar aria-label="User" className={classes.avatar}>
                Y
              </Avatar>
            }
            /**action={
              //<AccountsUIWrapper />
            //  <IconButton>
            //    <MoreVertIcon />
            //  </IconButton>
            }**/
            title={this.props.currentUser?this.props.currentUser.username:'Yono'}
            //subheader="createdAt"
          >

          </CardHeader>
          
          {/**<CardMedia
            className={classes.media}
            //image= {window.location.href.split('/')[2]+'/images/ishan-seefromthesky-566928-unsplash.jpg'}
            //image= {window.location.protocol+'://'+window.location.host+'/images/ishan-seefromthesky-566928-unsplash.jpg'}
            image= {'http://'+window.location.host+'/images/ishan-seefromthesky-566928-unsplash.jpg'}
            title='Queue'
          ></CardMedia>**/}
          
          <CardContent>
            {/**<Typography component="p" className={classes.contentMessage} >
              To join the queue, open a QR reading App on your phone and read the QR code given here or join at this url {queueUrl}.
            </Typography>**/}
          <AccountsUIWrapper className={classes.auth}/>  
            { this.props.currentUser ?
            <form className="new-queue" onSubmit={this.handleSubmit.bind(this)} >
              <TextField
                id="new-queue-name"
                //ref="newQueueName"
                label="Create New Queue"
                className={classes.textField}
                value={this.state.newQueueName}
                onChange={this.onNewQueueNameChange.bind(this)}
                margin="normal"
                //variant="outlined"
              />
              {/**<input
                type="text"
                ref="textInput"
                placeholder="Type to add new queues"
              />**/}
            </form> : ''
            }
            <Divider />
            <List>
              {this.renderQueues()}
            </List>
          </CardContent>

        </Card>
      </div>
    );
  }

}

export default withStyles(styles)(withTracker(() => {
  Meteor.subscribe('queues');
  const user = Meteor.isServer ? null : Meteor.user();
  const userId = Meteor.isServer ? null : Meteor.userId();

  return {
    queues: Queues.find({owner:userId}, { sort: { createdAt: -1 } }).fetch(),
    activeQueues: Queues.find({owner:userId}, { sort: { createdAt: -1 } }).count(),
    currentUser: user,
    currentUserId:userId
    
  };
})(App));
