import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import classnames from 'classnames';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { statusWait, statusServing, statusServed, statusLeft } from '../api/clients.js';

import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import FaceIcon from '@material-ui/icons/Face';
import { withRouter } from 'react-router-dom'

// Client component - represents a single client in a queue
class Client extends Component {

  deleteThisClient() {
    Meteor.call('clients.remove', this.props.client._id);
  }
  
  callThisClient(){
    //this.props.history.push('client/'+this.props.client._id)
    Meteor.call('clients.setStatus', this.props.client._id, statusServing);
  }
  
  servedClient(){
    Meteor.call('clients.setStatus', this.props.client._id, statusServed);
  }

  render() {
    return (
      <ListItem>
        <Avatar>
          <FaceIcon/>
        </Avatar>
        <ListItemText primary={this.props.client._id} secondary={this.props.client.status}/>
        <Button variant="outlined" color="primary" onClick={this.callThisClient.bind(this)}>
        Call
        </Button>
        <Button variant="outlined" color="secondary" onClick={this.servedClient.bind(this)}>
        Served
        </Button>
      </ListItem>
  
    );
  }
}
export default withRouter(Client);
