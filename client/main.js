import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import '../imports/startup/client/accounts-config.js';
import {renderRoutes} from '../imports/startup/client/routes.js';
import App from '../imports/ui/App.js';

//The main entry point to UI
Meteor.startup(() => {
  render(renderRoutes(), document.getElementById('render-target'));
  
});
