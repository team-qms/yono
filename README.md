# Yono Queue Management System

The system allow users to make queues which can be joined by pointing a smart QR reading device at the QR-code associated with a queue.

## Technology Stack

The project has been developed using MeteorJS. The frontend uses ReactJS with material ui componenet. The backend uses MongoDB to store data.

* [MeteroJS](https://www.meteor.com/) - Meteor is an open source platform for web, mobile, and desktop. 
* [Material UI](https://material-ui.com/) - React components that implement Google's Material Design.

## Authors

**Hadaytullah Kundi** 

## Development Environment Setup

* Install nodejs 

* clone the repo

* Install meteor

```
curl https://install.meteor.com/ | sh
```

* Install node packages

```
npm install
```

* Run the project

```
meteor run
```

## Credits

* Photo by Ishan @seefromthesky on Unsplash
* Photo by Ivana Cajina on Unsplash
